import { Component } from '@angular/core';

import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';
import 'firebase/database';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage {
  checked: string;
  response: any;
  state: string;
  uses: number;
  dispense: string;
  itemRef: AngularFireObject<any>;
  item: Observable<any>;
  color: string;

  constructor(db: AngularFireDatabase) {
    this.itemRef = db.object('pastillas');
    this.item = this.itemRef.valueChanges();
    this.itemRef.snapshotChanges().subscribe(action => {
      console.log(action.payload.val());
      this.response = action.payload.val();
      this.state = this.response.estado;
      this.uses = this.response.usos;
      this.dispense = this.response.dispensar;
      this.checked = this.state === 'on' ? 'true' : 'false';
      this.color = this.state === 'on' ? 'warning' : 'medium';
    });
  }

  changeState() {
    if (this.state === 'on') {
      this.itemRef.update({ estado: 'off'});
      console.log('turned off');
    } else {
      this.itemRef.update({ estado: 'on' });
      console.log('turned on');
    }
  }

  changeDispense() {
    if (this.dispense === 'no') {
      this.itemRef.update({ dispensar: 'yes' });
      console.log('Suministrar pastilla');
    } else {
      console.log('Espere a que suministre pastila');
    }
  }
}

